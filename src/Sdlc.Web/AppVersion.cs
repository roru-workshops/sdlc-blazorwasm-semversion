﻿using System;
using System.Reflection;

namespace Sdlc.Web
{
    public class AppVersion
    {
        public AppVersion(string versionString)
        {
            VersionString = versionString;
        }

        public string VersionString { get; }

        public static AppVersion GetCurrentVersion(Assembly assembly)
        {
            var aiv = assembly.GetCustomAttribute<AssemblyInformationalVersionAttribute>();
            return new AppVersion(aiv.InformationalVersion);
        }
    }
}
