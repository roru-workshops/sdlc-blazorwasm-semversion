# SDLC blazorwasm project

This project illustrates how we can easily configure CI/CD and SDLC processes using .NET.

SDLC - **S**oftware **D**evelopment **L**ife **C**ycle ([Wiki](https://en.wikipedia.org/wiki/Software_development_process)).

![sdlc-diagram.png](img/sdlc-diagram.png)

## Requirements

- .NET Core SDK 3.1 [download](https://dotnet.microsoft.com/download/dotnet-core/3.1)
- Blazor WASM template ([Get Started](https://docs.microsoft.com/en-us/aspnet/core/blazor/get-started?view=aspnetcore-3.1&tabs=visual-studio))
- Visual Studio (16.4 or 8.4.8 Mac) or Visual Studio Code

