# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.2.0] - 2020.04.15

## Added

- SDLC page

## [0.1.2] - 2020.04.15

### Fix

- Fix misspelling on index page
- Fix displaying version 

## [0.1.1] - 2020-03-29

### Changed

- Welcome page design

## [0.1.0] - 2020-03-27

### Addedd

- Main project structure
- UI design
- Version tracking
- CI/CD
- UI bugs for displaying hot fixes
